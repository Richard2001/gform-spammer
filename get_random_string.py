import random
string_array = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
string_length = random.randint(15, 30)
output = ""
for _ in range(string_length):
    output = output + string_array[random.randint(0, len(string_array) - 1)]
print(output)