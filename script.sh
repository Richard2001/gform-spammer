#!/bin/bash
counter=0
url="https://docs.google.com/forms/d/e/1FAIpQLSed03S6zFBN-Cd35ekzhYq7MJEjlYbzrp7SJdO5DIJ8JRTI3Q/formResponse"
sleep_time=1
while [ true ]; do
	rnd1=$(python3 get_random_string.py)
	rnd2=$(python3 get_random_string.py)
	rnd3=$(python3 get_random_string.py)
	rnd4=$(python3 get_random_string.py)

	ion1=$(python3 get_igen_or_nem.py)
	ion2=$(python3 get_igen_or_nem.py)
	ion3=$(python3 get_igen_or_nem.py)

	rint1=$(python3 get_random_int.py)
	rint2=$(python3 get_random_int.py)
	rint3=$(python3 get_random_int.py)
	rint4=$(python3 get_random_int.py)
	rint5=$(python3 get_random_int.py)
	rint6=$(python3 get_random_int.py)

	query="entry.110487200=${rnd1}&entry.1633736297=${rnd2}&entry.1453382751=${rnd3}&entry.730469139=${rnd4}&entry.57908982=${ion1}&entry.231228624=${rint1}&entry.312515374=${rint2}&entry.350887133=${rint3}&entry.647344544=${rint4}&entry.1545072158=${rint5}&entry.1727598938=${rint6}&entry.1176699170=${ion2}&entry.1540670366=${ion3}&entry.57908982_sentinel=&entry.231228624_sentinel=&entry.312515374_sentinel=&entry.350887133_sentinel=&entry.647344544_sentinel=&entry.1545072158_sentinel=&entry.1727598938_sentinel=&entry.1176699170_sentinel=&entry.1540670366_sentinel=&fvv=1&partialResponse=%5Bnull%2Cnull%2C%22-7796079078391647036%22%5D&pageHistory=0&fbzx=-7796079078391647036"
	
	full_url="${url}?${query}"
	
	curl -s -X POST $full_url -o /dev/null
	result=$?
	if [ $result -ne 0 ]; then
		break
	fi
	counter=$(($counter+1))
	echo "Spam count: $counter"
	echo -e "URL string:\n${full_url}\n"
	sleep $sleep_time
done

